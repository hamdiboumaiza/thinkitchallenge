package com.hb.thinkit.models

data class RoomModel(val image: Int, val title: String, val devices: String)