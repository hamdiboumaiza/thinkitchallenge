package com.hb.thinkit.ui.home

import androidx.lifecycle.ViewModel
import com.hb.thinkit.R
import com.hb.thinkit.models.RoomModel

class HomeViewModel : ViewModel() {

    fun getListOfRooms() =
        listOf(
            RoomModel(R.drawable.livingroom, "Living Room", "4 devices"),
            RoomModel(R.drawable.mediaroom, "Media Room", "6 devices"),
            RoomModel(R.drawable.bathroom, "Bathroom", "1 device"),
            RoomModel(R.drawable.bedroom, "Bedroom", "3 devices")
        )
}