package com.hb.thinkit.ui.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.hb.thinkit.utils.DATE_FORMAT
import com.hb.thinkit.R
import com.hb.thinkit.utils.USER_NAME
import com.hb.thinkit.databinding.ActivityHomeBinding
import java.text.SimpleDateFormat
import java.util.*

class HomeActivity : AppCompatActivity() {

    private val viewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(HomeViewModel::class.java)
    }
    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initViews()
    }

    private fun initViews() {
        getIntentExtras()?.let { username ->
            binding.tvWelcomeUser.text = getString(R.string.welcome_name, username)
        }

        binding.tvDate.text = getCurrentDate()

        with(binding.rvRooms) {
            setHasFixedSize(true)
            adapter = ListRoomsAdapter(viewModel.getListOfRooms())
        }
    }

    private fun getIntentExtras() = intent?.extras?.getString(USER_NAME)
    private fun getCurrentDate() = SimpleDateFormat(DATE_FORMAT, Locale.getDefault()).format(Date())
        .toUpperCase(Locale.ENGLISH)
}