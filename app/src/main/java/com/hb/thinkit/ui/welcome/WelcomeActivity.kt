package com.hb.thinkit.ui.welcome

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import com.hb.thinkit.BuildConfig
import com.hb.thinkit.R
import com.hb.thinkit.utils.USER_NAME
import com.hb.thinkit.databinding.ActivityWelcomeBinding
import com.hb.thinkit.ui.home.HomeActivity

class WelcomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWelcomeBinding
    private val preference by lazy { getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWelcomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //check if the user has already inserted a name
        getUsernameFromSharedPrefs()?.let { goToHomeActivity(it) }

        binding.btnContinue.setOnClickListener {
            val username = binding.etName.editableText.toString()
            if (username.isNotEmpty()) {
                saveUsernameFromSharedPrefs(username)
                goToHomeActivity(username)
            } else {
                showError()
            }
        }
    }

    private fun showError() = Toast.makeText(this, getString(R.string.welcome_error_message_name), Toast.LENGTH_SHORT).show()


    //redirection to the list of rooms activity
    private fun goToHomeActivity(username: String) {
        Intent(this, HomeActivity::class.java).apply {
            putExtra(USER_NAME, username)
            startActivity(this)
            finish()
        }
    }

    private fun saveUsernameFromSharedPrefs(username: String) =
        preference.edit { putString(USER_NAME, username) }

    private fun getUsernameFromSharedPrefs() = preference.getString(USER_NAME, null)

}