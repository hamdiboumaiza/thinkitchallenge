package com.hb.thinkit.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hb.thinkit.databinding.ItemRoomBinding
import com.hb.thinkit.models.RoomModel


class ListRoomsAdapter(private val list: List<RoomModel>) :
    RecyclerView.Adapter<ListRoomsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ItemRoomBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindTo(list[position])
    }

    override fun getItemCount() = list.size

    inner class ViewHolder(private val view: ItemRoomBinding) : RecyclerView.ViewHolder(view.root) {
        fun bindTo(roomModel: RoomModel) {
            with(view) {
                imgItemRoom.setImageResource(roomModel.image)
                tvItemRoomType.text = roomModel.title
                tvItemDevices.text = roomModel.devices
            }
        }
    }
}